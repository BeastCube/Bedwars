package net.beastcube.bedwars;

import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import net.beastcube.api.bukkit.config.annotation.Setting;
import net.beastcube.api.bukkit.config.annotation.SettingBase;

/**
 * @author BeastCube
 */
@SettingBase("bedwars")
public class BedwarsConfig extends AnnotationConfiguration {
    @Setting
    public int numberOfTeams = 4;
}