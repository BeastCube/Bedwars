package net.beastcube.bedwars;

import net.beastcube.minigameapi.team.Team;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.inventory.Inventory;

/**
 * @author BeastCube
 */
public class BedwarsTeam extends Team {

    private final Inventory teamchest = Bukkit.createInventory(null, 27, getChatColor() + "Teamkiste " + getName());
    private boolean hasbed = true;

    public BedwarsTeam(String name, Color color) {
        super(name, color);
    }

    @Override
    public String getScoreboardDisplayName() {
        return (hasBed() ? ChatColor.GREEN + "✔ " : ChatColor.RED + "✖ ") + getDisplayName();
    }

    public boolean hasBed() {
        return hasbed;
    }

    public void setHasBed(boolean hasBed) {
        this.hasbed = hasBed;
    }

    public boolean isBed(Location loc) {
        return Bedwars.getMinigameAPI().getTeam(Bedwars.getMinigameAPI().getArena().isBed(loc)) == this;
    }

    public Inventory getTeamChest() {
        return teamchest;
    }
}
