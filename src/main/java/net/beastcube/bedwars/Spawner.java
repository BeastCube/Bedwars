package net.beastcube.bedwars;

import org.bukkit.Bukkit;

/**
 * @author BeastCube
 */
public class Spawner {
    public static void start() {
        startBronze();
        startSilver();
        startGold();
    }

    private static void startBronze() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Bedwars.getInstance(), Spawner::spawnBronze, getArena().getBronzeFreq(), getArena().getBronzeFreq());
    }

    private static void startSilver() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Bedwars.getInstance(), Spawner::spawnSilver, getArena().getSilverFreq(), getArena().getSilverFreq());
    }

    private static void startGold() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Bedwars.getInstance(), Spawner::spawnGold, getArena().getGoldFreq(), getArena().getGoldFreq());
    }

    public static void spawnBronze() {
        getArena().getBronze().forEach(l -> l.getWorld().dropItem(l, Bedwars.getBronze(1)));
    }

    public static void spawnSilver() {
        getArena().getSilver().forEach(l -> l.getWorld().dropItem(l, Bedwars.getSilver(1)));
    }

    public static void spawnGold() {
        getArena().getGold().forEach(l -> l.getWorld().dropItem(l, Bedwars.getGold(1)));
    }

    public static BedwarsArena getArena() {
        return Bedwars.getMinigameAPI().getArena();
    }

}
