package net.beastcube.bedwars.shop;

import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.bedwars.Bedwars;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * @author BeastCube
 */
public class Merchant {
    ArrayList<TradingRecipe> recipes = new ArrayList<TradingRecipe>();
    String title;

    public Merchant(String t) {
        title = t;
    }

    public Merchant() {
        title = "Shop";
    }

    public void addRecipe(TradingRecipe recipe) {
        recipes.add(recipe);
    }

    public TradingRecipe getRecipe(int i) {
        return recipes.get(i);
    }

    public void clearRecipes() {
        recipes.clear();
    }

    public void setTitle(String t) {
        title = t;
    }

    public void openTrading(Player p) {
        //inner.openTrading(((CraftPlayer)p).getHandle());
        ItemMenu menu = new ItemMenu(title, ItemMenu.Size.fit(recipes.size()), Bedwars.getMinigameAPI().getMinigame());
        for (int i = 0; i < recipes.size(); i++) {
            menu.setItem(i, recipes.get(i));
        }
        menu.open(p);
    }

    public String getTitle() {
        return title;
    }
}
