package net.beastcube.bedwars.shop;

import org.bukkit.DyeColor;

/**
 * @author BeastCube
 */
public enum ShopMode {
    VILLAGER("Villager-Style", DyeColor.LIGHT_BLUE),
    ONE_CLICK("Kisten-Style", DyeColor.LIME);

    private String name;
    private DyeColor color;

    ShopMode(String name, DyeColor color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public DyeColor getColor() {
        return color;
    }

    public ShopMode getOther() {
        return (this == VILLAGER) ? ONE_CLICK : VILLAGER;
    }
}
