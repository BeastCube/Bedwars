package net.beastcube.bedwars.shop.nms;

import net.beastcube.bedwars.shop.TradingRecipe;
import net.minecraft.server.v1_8_R3.*;

import java.util.ArrayList;

/**
 * @author BeastCube
 */
public class NMSMerchant implements IMerchant {

    EntityHuman tradingPlayer;
    ArrayList<TradingRecipe> tradingRecipes;

    public NMSMerchant(ArrayList<TradingRecipe> tradingRecipes, EntityHuman tradingPlayer) {
        this.tradingPlayer = tradingPlayer;
        this.tradingRecipes = tradingRecipes;
    }

    @Override
    public void a_(EntityHuman entityHuman) {
        tradingPlayer = entityHuman;
    }

    @Override
    public EntityHuman v_() {
        return tradingPlayer;
    }

    @Override
    public MerchantRecipeList getOffers(EntityHuman entityHuman) {
        MerchantRecipeList list = new MerchantRecipeList();
        for (TradingRecipe t : tradingRecipes) {
            list.add(t.toMerchantRecipe());
        }
        return list;
    }

    @Override
    public void a(MerchantRecipe merchantRecipe) {
        //When something is bought
    }

    @Override
    public void a_(ItemStack itemStack) {
        //When something is being bought
    }

    @Override
    public IChatBaseComponent getScoreboardDisplayName() {
        return null;
    }
}
