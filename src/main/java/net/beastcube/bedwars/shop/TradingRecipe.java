package net.beastcube.bedwars.shop;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.InventoryWorkaround;
import net.minecraft.server.v1_8_R3.MerchantRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

/**
 * @author BeastCube
 */
public class TradingRecipe extends MenuItem {
    private ItemStack in, out;

    private static String getDisplayName(ItemStack stack) {
        if (stack.hasItemMeta()) {
            return stack.getItemMeta().getDisplayName();
        } else {
            return stack.getType().toString();
        }
    }

    public TradingRecipe(ItemStack in, ItemStack out) {
        super(getDisplayName(out), out, in.getAmount() + " " + getDisplayName(in));
        this.in = in;
        this.out = out;
    }

    public ItemStack getIn() {
        return in;
    }

    public ItemStack getOut() {
        return out;
    }

    public void setIn(ItemStack val) {
        in = val;
    }

    public void setOut(ItemStack val) {
        out = val;
    }

    private static boolean containsAtLeast(Inventory inventory, ItemStack itemStack) {
        ItemStack[] stack = inventory.getContents();
        int tamount = 0;
        for (ItemStack i : stack) {
            if (i != null) {
                if (i.isSimilar(itemStack)) {
                    tamount += i.getAmount();
                    if (tamount >= itemStack.getAmount())
                        return true;
                }
            }
        }
        return false;
    }

    private static ItemStack getAll(ItemStack itemStack, Inventory inv) {
        ItemStack stack = new ItemStack(itemStack);
        stack.setAmount(0);
        for (ItemStack i : inv.getContents()) {
            if (i != null)
                if (i.isSimilar(itemStack))
                    stack.setAmount(stack.getAmount() + i.getAmount());
        }
        return stack;
    }

    @Override
    public void onItemClick(ItemClickEvent event) {
        super.onItemClick(event);
        Player p = event.getPlayer();
        if (event.getClickType().isShiftClick()) {
            if (!containsAtLeast(p.getInventory(), in)) {
                p.playSound(p.getLocation(), Sound.ANVIL_USE, 5F, 5F);
                p.sendMessage(ChatColor.RED + "Nicht genug " + getDisplayName(in));
            } else {
                ItemStack money = getAll(in, p.getInventory());
                int amount = money.getAmount() / in.getAmount();
                //TODO max amount
                //TODO more than 1 stack isn't working the way it should
                ItemStack _out = new ItemStack(out);
                _out.setAmount(out.getAmount() * amount);
                Map<Integer, ItemStack> map = InventoryWorkaround.addAllItems(p.getInventory(), new ItemStack(_out));

                if (map.isEmpty()) {
                    ItemStack is = new ItemStack(in);
                    is.setAmount(in.getAmount() * amount);
                    p.getInventory().removeItem(is);
                    p.playSound(p.getLocation(), Sound.ORB_PICKUP, 5F, 5F);
                } else {
                    int rest = map.get(0).getAmount();
                    if (rest > _out.getAmount() - out.getAmount()) {
                        p.playSound(p.getLocation(), Sound.ANVIL_USE, 5F, 5F);
                        p.sendMessage(ChatColor.RED + "Nicht genug Platz im Inventar!");
                    } else {
                        //space for one or more
                        int space = (_out.getAmount() - rest) / out.getAmount();
                        _out.setAmount(out.getAmount() * space);
                        InventoryWorkaround.addAllItems(p.getInventory(), new ItemStack(_out)); //no validation needed anymore
                        ItemStack is = new ItemStack(in);
                        is.setAmount(in.getAmount() * space);
                        p.getInventory().removeItem(is);
                        p.playSound(p.getLocation(), Sound.ORB_PICKUP, 5F, 5F);
                    }
                }
            }
        } else {
            if (containsAtLeast(p.getInventory(), in)) {
                if (InventoryWorkaround.addAllItems(p.getInventory(), new ItemStack(out)).isEmpty()) {
                    p.getInventory().removeItem(new ItemStack(in));
                    p.playSound(p.getLocation(), Sound.ORB_PICKUP, 5F, 5F);
                } else {
                    p.playSound(p.getLocation(), Sound.ANVIL_USE, 5F, 5F);
                    p.sendMessage(ChatColor.RED + "Nicht genug Platz im Inventar!");
                }
            } else {
                p.playSound(p.getLocation(), Sound.ANVIL_USE, 5F, 5F);
                p.sendMessage(ChatColor.RED + "Nicht genug " + getDisplayName(in));
            }
        }
    }

    public MerchantRecipe toMerchantRecipe() {
        return new MerchantRecipe(CraftItemStack.asNMSCopy(in), CraftItemStack.asNMSCopy(out));
    }
}
