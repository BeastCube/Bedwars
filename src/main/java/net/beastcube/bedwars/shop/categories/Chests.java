package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Chests {

    public static final ItemStack LITTLECHEST = new ItemBuilder(new ItemStack(Material.DROPPER, 1, (short) 0)).name(ChatColor.DARK_PURPLE + "Kleine Kiste").build();
    public static final ItemStack CHEST = new ItemBuilder(new ItemStack(Material.CHEST, 1, (short) 0)).name(ChatColor.DARK_PURPLE + "Kiste").build();
    public static final ItemStack ENDERCHEST = new ItemBuilder(new ItemStack(Material.ENDER_CHEST, 1, (short) 0)).name(ChatColor.DARK_PURPLE + "Ender-Kiste").build();


    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Kisten", new ItemStack(Material.CHEST));

        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(10), LITTLECHEST));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(1), CHEST));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(1), ENDERCHEST));

        return inv;
    }
}
