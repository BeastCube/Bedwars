package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Food {

    public static final ItemStack ROTTENFLESH = new ItemBuilder(new ItemStack(Material.ROTTEN_FLESH, 16, (short) 0)).name(ChatColor.DARK_RED + "Verrottetes Fleisch").build();
    public static final ItemStack COOKIE = new ItemBuilder(new ItemStack(Material.COOKIE, 1, (short) 0)).name(ChatColor.GOLD + "Keks").build();
    public static final ItemStack BREAD = new ItemBuilder(new ItemStack(Material.BREAD, 2, (short) 0)).name(ChatColor.GOLD + "Brot").build();
    public static final ItemStack STEAK = new ItemBuilder(new ItemStack(Material.COOKED_BEEF, 1, (short) 0)).name(ChatColor.RED + "T-Bone Steak").build();
    public static final ItemStack APPLES = new ItemBuilder(new ItemStack(Material.APPLE, 4, (short) 0)).name(ChatColor.RED + "Apfel").build();
    public static final ItemStack GOLDENAPPLE = new ItemBuilder(new ItemStack(Material.GOLDEN_APPLE, 1, (short) 0)).name(ChatColor.GOLD + "Goldener Apfel").build();


    @SuppressWarnings("unused")
    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Essen", new ItemStack(Material.BREAD));

        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(1), ROTTENFLESH));
        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(1), COOKIE));
        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(4), BREAD));
        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(6), STEAK));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(1), APPLES));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(10), GOLDENAPPLE));

        return inv;
    }
}
