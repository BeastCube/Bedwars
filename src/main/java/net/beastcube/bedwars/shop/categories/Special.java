package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.items.BasePorter;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Special {

    public static final ItemStack TNT = new ItemBuilder(new ItemStack(Material.TNT, 1, (short) 0)).name(ChatColor.RED + "TNT").build();
    public static final ItemStack FLINTANDSTEEL = new ItemBuilder(new ItemStack(Material.FLINT_AND_STEEL, 1, (short) 0)).name(ChatColor.GOLD + "Feuerzeug").build();
    public static final ItemStack ENDERPEARL = new ItemBuilder(new ItemStack(Material.ENDER_PEARL, 1, (short) 0)).name(ChatColor.DARK_BLUE + "Enderperle").build();
    public static final ItemStack LAUNCHBOW = new ItemBuilder(new ItemStack(Material.BOW, 1, (short) 5)).name(ChatColor.DARK_PURPLE + "Wurfbogen").build();
    public static final ItemStack ICE = new ItemBuilder(new ItemStack(Material.ICE, 1, (short) -1)).name(ChatColor.BLUE + "Eis").build();
    public static final ItemStack SNOWBALLS = new ItemBuilder(new ItemStack(Material.SNOW_BALL, 16, (short) 0)).name(ChatColor.BLUE + "Schneebälle").build();
    public static final ItemStack SHEEP = new ItemBuilder(new ItemStack(Material.MONSTER_EGG, 1, (short) 0)).name(ChatColor.LIGHT_PURPLE + "In ya face, MÄÄHHN!").build();
    public static final ItemStack BASEPORTER = BasePorter.getBasePorter().createItemStack();
    public static final ItemStack KAMIKAZE = new ItemBuilder(Material.LEATHER_CHESTPLATE).name(ChatColor.DARK_RED + "Kamikaze - Hemd").build();

    @SuppressWarnings("unused")
    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Spezial", new ItemStack(Material.TNT));

        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(1), TNT));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(2), FLINTANDSTEEL));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(12), ENDERPEARL));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(40), LAUNCHBOW));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(2), ICE));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(5), SNOWBALLS));
        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(64), SHEEP));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(1), BASEPORTER));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(16), KAMIKAZE));

        return inv;
    }
}