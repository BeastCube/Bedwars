package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Tools {

    public static final ItemStack WOODENPICK = new ItemBuilder(new ItemStack(Material.WOOD_PICKAXE, 1, (short) -1)).name(ChatColor.DARK_RED + "Holzspitzhacke").build();
    public static final ItemStack STONEPICK = new ItemBuilder(new ItemStack(Material.STONE_PICKAXE, 1, (short) -1)).name(ChatColor.DARK_GRAY + "Steinspitzhacke").build();
    public static final ItemStack IRONPICK = new ItemBuilder(new ItemStack(Material.IRON_PICKAXE, 1, (short) -1)).name(ChatColor.GRAY + "Eisenspitzhacke").build();
    public static final ItemStack DIAMONDPICK = new ItemBuilder(new ItemStack(Material.DIAMOND_PICKAXE, 1, (short) -1)).name(ChatColor.GRAY + "Diamantspitzhacke").build();
    public static final ItemStack GOLDPICK = new ItemBuilder(new ItemStack(Material.GOLD_PICKAXE, 1, (short) -1)).name(ChatColor.GOLD + "Goldspitzhacke")
            .enchantment(Enchantment.DIG_SPEED, 2).build();
    public static final ItemStack GOLDPICK2 = new ItemBuilder(new ItemStack(Material.GOLD_PICKAXE, 1, (short) -1)).name(ChatColor.GOLD + "Goldspitzhacke")
            .enchantment(Enchantment.DIG_SPEED, 2).enchantment(Enchantment.DURABILITY, 2).build();

    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Werkzeuge", new ItemStack(Material.IRON_PICKAXE));

        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(3), WOODENPICK));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(1), STONEPICK));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(5), IRONPICK));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(1), DIAMONDPICK));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(5), GOLDPICK));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(7), GOLDPICK2));

        return inv;
    }
}