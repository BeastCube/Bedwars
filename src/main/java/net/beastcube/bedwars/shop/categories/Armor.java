package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Armor {

    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Rüstung", new ItemStack(Material.IRON_CHESTPLATE));

        Team team = Bedwars.getMinigameAPI().getPlayersTeam(p);
        if (team != null) {
            ItemStack leatherhelmet = new ItemBuilder(Material.LEATHER_HELMET).amount(1).durability(-1).name(team.getChatColor() + "Lederhelm").color(team.getColor()).build();
            ItemStack leatherleggings = new ItemBuilder(Material.LEATHER_LEGGINGS).amount(1).durability(-1).name(team.getChatColor() + "Lederhose").color(team.getColor()).build();
            ItemStack leatherboots = new ItemBuilder(Material.LEATHER_BOOTS).amount(1).durability(-1).name(team.getChatColor() + "Lederschuhe").color(team.getColor()).build();

            inv.addRecipe(new TradingRecipe(Bedwars.getBronze(2), leatherhelmet));
            inv.addRecipe(new TradingRecipe(Bedwars.getBronze(2), leatherleggings));
            inv.addRecipe(new TradingRecipe(Bedwars.getBronze(2), leatherboots));
        }


        ItemStack goldchest = new ItemBuilder(new ItemStack(Material.GOLD_CHESTPLATE, 1, (short) -1)).name(ChatColor.GOLD + "Goldbrustplatte").build();
        ItemStack ironchest = new ItemBuilder(new ItemStack(Material.IRON_CHESTPLATE, 1, (short) -1)).name(ChatColor.DARK_GRAY + "Stahlbrustplatte").build();
        ItemStack enchironchest = new ItemBuilder(new ItemStack(Material.IRON_CHESTPLATE, 1, (short) -1)).name(ChatColor.DARK_GRAY + "Verbesserte Stahlbrustplatte").
                enchantment(Enchantment.PROTECTION_ENVIRONMENTAL).enchantment(Enchantment.PROTECTION_FIRE).build();
        ItemStack thorns = new ItemBuilder(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1, (short) -1)).name(ChatColor.GRAY + "Stachelhemd").
                enchantment(Enchantment.THORNS, 5).build();
        ItemStack superchest = new ItemBuilder(new ItemStack(Material.IRON_CHESTPLATE, 1, (short) -1)).name(ChatColor.GRAY + "Titaniumbrustplatte").
                enchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3).enchantment(Enchantment.PROTECTION_PROJECTILE, 2).enchantment(Enchantment.PROTECTION_FIRE, 1).build();
        ItemStack gummiboots = new ItemBuilder(new ItemStack(Material.GOLD_BOOTS, 1, (short) -1)).name(ChatColor.GRAY + "Gummischuhe")
                .enchantment(Enchantment.PROTECTION_FALL, 2).build();

        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(1), goldchest));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(10), ironchest));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(3), enchironchest));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(3), thorns));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(20), superchest));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(1), gummiboots));

        return inv;
    }
}