package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import net.beastcube.minigameapi.team.ColorConverter;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Blocks {

    public static final ItemStack ENDSTONE = new ItemBuilder(Material.ENDER_STONE).amount(2)/*.name(ChatColor.DARK_GRAY + "Endstein")*/.build();
    public static final ItemStack IRONBLOCK = new ItemBuilder(Material.IRON_BLOCK).amount(2)/*.name(ChatColor.DARK_GRAY + "Eisenblock")*/.build();
    public static final ItemStack LADDER = new ItemBuilder(Material.LADDER).amount(3)/*.name(ChatColor.DARK_GRAY + "Leiter")*/.build();

    @SuppressWarnings({"unused", "deprecation"})
    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Baumaterial", new ItemStack(Material.SANDSTONE));

        Team team = Bedwars.getMinigameAPI().getPlayersTeam(p);
        if (team != null) {
            DyeColor color = ColorConverter.colorToDyeColor(team.getColor());

            ItemStack BLOCK = new ItemBuilder(Material.STAINED_CLAY).amount(2).dyeColor(color)/*.name("Blöcke")*/.build(); //TODO no name because if you collect the broken blocks, they have not the same name as the blocks you can buy and therefore you cannot stack them
            inv.addRecipe(new TradingRecipe(Bedwars.getBronze(1), BLOCK));

            inv.addRecipe(new TradingRecipe(Bedwars.getBronze(5), ENDSTONE));
            inv.addRecipe(new TradingRecipe(Bedwars.getSilver(1), IRONBLOCK));

            ItemStack GLASS = new ItemBuilder(Material.STAINED_GLASS).dyeColor(color)/*.name("Team-Blöcke")*/.build(); //Will get destroyed if someone from another team runs above them
            inv.addRecipe(new TradingRecipe(Bedwars.getBronze(2), GLASS));
        }
        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(5), LADDER));

        return inv;
    }
}
