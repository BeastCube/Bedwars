package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Bows {

    public static final ItemStack ADVANCED = new ItemBuilder(new ItemStack(Material.BOW, 1, (short) -1)).name(ChatColor.GOLD + "Bogen").enchantment(Enchantment.ARROW_INFINITE).build();
    public static final ItemStack POWERBOW = new ItemBuilder(new ItemStack(Material.BOW, 1, (short) -1)).name(ChatColor.GOLD + "Stärke-Bogen").
            enchantment(Enchantment.ARROW_INFINITE).enchantment(Enchantment.ARROW_DAMAGE).build();
    public static final ItemStack KNOCKBACKBOW = new ItemBuilder(new ItemStack(Material.BOW, 1, (short) -1)).name(ChatColor.GOLD + "Rückstoss-Bogen").
            enchantment(Enchantment.ARROW_INFINITE).enchantment(Enchantment.ARROW_KNOCKBACK, 1).build();
    public static final ItemStack SUPERBOW = new ItemBuilder(new ItemStack(Material.BOW, 1, (short) -1)).name(ChatColor.GOLD + "Super-Bogen").
            enchantment(Enchantment.ARROW_INFINITE).enchantment(Enchantment.ARROW_DAMAGE).enchantment(Enchantment.ARROW_KNOCKBACK, 1).build();
    public static final ItemStack ARROW = new ItemBuilder(new ItemStack(Material.ARROW, 1, (short) -1)).name(ChatColor.GOLD + "Pfeil").build();


    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Bögen", new ItemStack(Material.BOW));

        inv.addRecipe(new TradingRecipe(Bedwars.getGold(3), ADVANCED));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(7), POWERBOW));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(7), KNOCKBACKBOW));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(13), SUPERBOW));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(1), ARROW));

        return inv;
    }
}