package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Trading {

    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Handel", new ItemStack(Material.EMERALD));

        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(16), Bedwars.getSilver(1)));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(1), Bedwars.getBronze(14)));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(16), Bedwars.getGold(1)));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(1), Bedwars.getSilver(14)));

        return inv;
    }
}
