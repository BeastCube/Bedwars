package net.beastcube.bedwars.shop.categories;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.ShopMenuItem;
import net.beastcube.bedwars.shop.TradingRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class Weapons {

    public static final ItemStack KNUPPEL = new ItemBuilder(new ItemStack(Material.STICK, 1, (short) 0)).name(ChatColor.DARK_GREEN + "Knüppel")
            .enchantment(Enchantment.KNOCKBACK, 1).build();
    public static final ItemStack WOODSWORD = new ItemBuilder(new ItemStack(Material.WOOD_SWORD, 1, (short) -1)).name(ChatColor.DARK_GREEN + "Holzschwert").build();
    public static final ItemStack STONESWORD = new ItemBuilder(new ItemStack(Material.IRON_SWORD, 1, (short) -1)).name(ChatColor.GRAY + "Eisenschwert").build();
    public static final ItemStack IRONAXE = new ItemBuilder(new ItemStack(Material.IRON_AXE, 1, (short) 0)).name(ChatColor.DARK_GREEN + "Zerschredderer").enchantment(Enchantment.DAMAGE_ALL, 1).build();
    public static final ItemStack ROD = new ItemBuilder(new ItemStack(Material.FISHING_ROD, 1, (short) -1)).name(ChatColor.LIGHT_PURPLE + "Angel").build();


    public static MenuItem getCategoryItem(Player p) {
        ShopMenuItem inv = new ShopMenuItem("Waffen", new ItemStack(Material.DIAMOND_SWORD));

        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(5), KNUPPEL));
        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(30), WOODSWORD));
        inv.addRecipe(new TradingRecipe(Bedwars.getSilver(5), STONESWORD));
        inv.addRecipe(new TradingRecipe(Bedwars.getGold(3), IRONAXE));
        inv.addRecipe(new TradingRecipe(Bedwars.getBronze(32), ROD));

        return inv;
    }

}