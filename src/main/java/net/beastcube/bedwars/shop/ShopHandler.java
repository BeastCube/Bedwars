package net.beastcube.bedwars.shop;

import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.kits.Kits;
import net.beastcube.bedwars.shop.categories.*;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class ShopHandler {
    private static HashMap<UUID, ShopMode> modes = new HashMap<UUID, ShopMode>();
    private static HashMap<UUID, ItemMenu> shops = new HashMap<UUID, ItemMenu>();

    public static void showShop(Player p) {
        updateMenu(p).open(p);
    }

    public static void updateShop(Player p) {
        updateMenu(p).update(p);
    }

    private static ItemMenu updateMenu(Player p) {
        ItemMenu menu = new ItemMenu("Shop", ItemMenu.Size.THREE_LINE, Bedwars.getMinigameAPI().getMinigame());
        if (shops.containsKey(p.getUniqueId()) && shops.get(p.getUniqueId()) != null) {
            menu = shops.get(p.getUniqueId());
        }
        menu.setItem(0, Blocks.getCategoryItem(p));
        menu.setItem(1, Tools.getCategoryItem(p));
        menu.setItem(2, Weapons.getCategoryItem(p));
        menu.setItem(3, Armor.getCategoryItem(p));
        menu.setItem(4, Bows.getCategoryItem(p));
        menu.setItem(5, Chests.getCategoryItem(p));
        menu.setItem(6, Food.getCategoryItem(p));
        menu.setItem(7, Special.getCategoryItem(p));
        if (Bedwars.getMinigameAPI().getPlayerKit(p) == Kits.TRADER) {
            menu.setItem(8, Trading.getCategoryItem(p));
        }

        menu.setItem(22, new ToggleModeItem(p));

        shops.put(p.getUniqueId(), menu);
        return menu;
    }

    public static ItemMenu getShop(Player p) {
        return shops.get(p.getUniqueId());
    }

    public static void removeShop(Player p) {
        if (shops.containsKey(p.getUniqueId()))
            shops.remove(p.getUniqueId());
    }

    public static void setMode(Player p, ShopMode mode) {
        modes.put(p.getUniqueId(), mode);
    }

    public static ShopMode getMode(Player p) {
        ShopMode mode = modes.get(p.getUniqueId());
        return mode == null ? ShopMode.ONE_CLICK : mode;
    }
}
