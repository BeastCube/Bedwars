package net.beastcube.bedwars.shop;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class ToggleModeItem extends MenuItem {

    @SuppressWarnings("deprecation")
    public ToggleModeItem(Player player) {
        super(ChatColor.GREEN + ShopHandler.getMode(player).getName(), new ItemStack(Material.WOOL, 1, ShopHandler.getMode(player).getColor().getWoolData()), ChatColor.GRAY + "Schaltet den Modus um");
    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        ShopHandler.setMode(e.getPlayer(), ShopHandler.getMode(e.getPlayer()).getOther());
        ShopHandler.updateShop(e.getPlayer());
    }

}
