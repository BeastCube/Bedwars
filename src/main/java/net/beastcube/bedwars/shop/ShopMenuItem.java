package net.beastcube.bedwars.shop;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.bedwars.Bedwars;
import net.beastcube.bedwars.shop.nms.NMSMerchant;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * @author BeastCube
 */
public class ShopMenuItem extends MenuItem {

    ArrayList<TradingRecipe> recipes = new ArrayList<>();
    String name;

    public ShopMenuItem(String name, ItemStack icon) {
        super(name, icon);
        this.name = name;
    }

    public void addRecipe(TradingRecipe recipe) {
        recipes.add(recipe);
    }

    @Override
    public void onItemClick(final ItemClickEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Bedwars.getMinigameAPI().getMinigame(), () -> openTrading(event.getPlayer()), 1);
    }

    private void openTrading(Player p) {
        if (ShopHandler.getMode(p) == ShopMode.VILLAGER) {
            net.minecraft.server.v1_8_R3.EntityHuman human = ((CraftPlayer) p).getHandle();
            human.openTrade(new NMSMerchant(recipes, human));
        } else {
            ItemMenu submenu = new ItemMenu(name, ItemMenu.Size.fit(recipes.size()), Bedwars.getMinigameAPI().getMinigame());
            for (int i = 0; i < recipes.size(); i++) {
                submenu.setItem(i, recipes.get(i));
            }
            submenu.open(p);
        }
    }
}
