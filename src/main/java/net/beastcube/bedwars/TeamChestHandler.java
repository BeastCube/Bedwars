package net.beastcube.bedwars;

import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashMap;

/**
 * @author BeastCube
 */
public class TeamChestHandler implements Listener {
    public HashMap<Block, BedwarsTeam> chests = new HashMap<Block, BedwarsTeam>();

    public BedwarsTeam getTeamFromChest(Block chest) {
        if (chests.containsKey(chest)) {
            return chests.get(chest);
        }
        return null;
    }

    public void addChest(Block chest, BedwarsTeam team) {
        chests.put(chest, team);
    }

    public void removeChest(Block chest) {
        getTeamFromChest(chest).getTeamChest().getViewers().clear(); //TODO Test
        chests.remove(chest);
    }

    public boolean isChest(Block chest) {
        return chests.containsKey(chest);
    }

    public void openTeamchest(Block chest, Player p) {
        p.openInventory(getTeamFromChest(chest).getTeamChest());
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent e) {
        if (Bedwars.getMinigameAPI().getState() == MinigameState.INGAME && e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.ENDER_CHEST) {
                final Block chest = e.getClickedBlock();
                Team team = getTeamFromChest(chest);
                if (team != null && Bedwars.getMinigameAPI().getPlayerState(e.getPlayer()) == PlayerState.INGAME) {
                    e.setCancelled(true);
                    if (team != Bedwars.getMinigameAPI().getPlayersTeam(e.getPlayer())) {
                        e.getPlayer().sendMessage(ChatColor.RED + "Du kannst die Teamkiste von Team " + team.getChatColor() + team.getName() + ChatColor.RED + " nicht öffnen!");
                    } else {
                        Bukkit.getScheduler().runTask(Bedwars.getMinigameAPI().getMinigame(), new Runnable() {
                            @Override
                            public void run() {
                                openTeamchest(chest, e.getPlayer());
                            }
                        });
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.ENDER_CHEST) {
            if (isChest(e.getBlock())) {
                removeChest(e.getBlock());
                e.setCancelled(true);
                e.getBlock().setType(Material.AIR);
                BedwarsTeam team = getTeamFromChest(e.getBlock());
                for (Player p : team.getPlayers()) {
                    p.sendMessage(ChatColor.RED + "Eine deiner Teamchests wurde abgebaut!");
                }
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (Bedwars.getMinigameAPI().getState() == MinigameState.INGAME) {
            if (e.getBlock().getType() == Material.ENDER_CHEST) {
                BedwarsTeam team = (BedwarsTeam) Bedwars.getMinigameAPI().getPlayersTeam(e.getPlayer());
                if (team != null)
                    addChest(e.getBlock(), team);
            }
        }
    }

}
