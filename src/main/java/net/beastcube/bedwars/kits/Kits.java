package net.beastcube.bedwars.kits;

import net.beastcube.minigameapi.kits.ItemStackKit;
import net.beastcube.minigameapi.kits.KitChoosing;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author BeastCube
 */
public final class Kits {
    private static int id = -1;

    private static int getId() {
        id++;
        return id;
    }

    private static Inventory createInventory(ItemStack[] stacks) {
        Inventory inv = Bukkit.createInventory(null, InventoryType.PLAYER);
        for (int i = 0; i < stacks.length; i++)
            inv.setItem(i, stacks[i]);
        return inv;
    }

    public static final ItemStackKit TRADER = new ItemStackKit("Händler", "Kann im Shop 16 Bronze gegen 1 Silber und 16 Silber gegen 1 Gold tauschen.",
            Bukkit.createInventory(null, InventoryType.PLAYER), null, null, getId(), Material.EMERALD, (short) 0);
    public static final ItemStackKit ATHLETE = new ItemStackKit("Athlet", "Ist beweglicher als alle anderen, er läuft schneller und springt höher.",
            Bukkit.createInventory(null, InventoryType.PLAYER), null, new PotionEffect[]{
            new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0, false),
            new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 0, false)
    }, getId(), Material.FEATHER, (short) 0);
    public static final ItemStackKit MINER = new ItemStackKit("Miner", "Baut etwas schneller ab als alle anderen",
            Bukkit.createInventory(null, InventoryType.PLAYER), null, new PotionEffect[]{
            new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 1, false)
    }, getId(), Material.GOLD_PICKAXE, (short) 0);
    /*
    public static final ItemStackKit RUSHER = new ItemStackKit("Rusher", "Besitzt beim Spawnen bereits einen Knüppel, eine Holzspitzhacke und 5 Blöcke",
            createInventory(new ItemStack[]{
                    new ItemStack(Weapons.KNUPPEL), new ItemStack(Tools.WOODENPICK), new ItemBuilder(new ItemStack(Blocks.SANDSTONE)).amount(5).build() //TODO Player Specific Kits
            }), null, null, getId(), Material.STICK, (short) 0);
*/

    public static void register() {
        KitChoosing.registerKit(TRADER);
        KitChoosing.registerKit(ATHLETE);
        KitChoosing.registerKit(MINER);
        //KitChoosing.registerKit(RUSHER);
    }
}
