package net.beastcube.bedwars;

import lombok.Getter;
import net.beastcube.api.bukkit.fakemob.event.PlayerInteractFakeMobEvent;
import net.beastcube.api.bukkit.util.title.ActionBarAPI;
import net.beastcube.api.bukkit.util.title.TitleAPI;
import net.beastcube.api.bukkit.util.title.TitleObject;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.stats.Stat;
import net.beastcube.api.commons.stats.minigames.BedwarsStats;
import net.beastcube.bedwars.items.KamikazeListener;
import net.beastcube.bedwars.items.LaunchBowListener;
import net.beastcube.bedwars.items.sheep.Register;
import net.beastcube.bedwars.items.sheep.SheepListener;
import net.beastcube.bedwars.kits.Kits;
import net.beastcube.bedwars.shop.ShopHandler;
import net.beastcube.minigameapi.Minigame;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.countdowns.Countdowns;
import net.beastcube.minigameapi.defaults.Defaults;
import net.beastcube.minigameapi.defaults.PlayerAmountBoundType;
import net.beastcube.minigameapi.defaults.PlayerAmountBounds;
import net.beastcube.minigameapi.events.*;
import net.beastcube.minigameapi.kits.KitChooserInteractItem;
import net.beastcube.minigameapi.kits.KitChoosing;
import net.beastcube.minigameapi.maps.MapChooserInteractItem;
import net.beastcube.minigameapi.maps.MapChoosing;
import net.beastcube.minigameapi.scoreboard.ScoreboardManager;
import net.beastcube.minigameapi.scoreboard.ScoreboardMode;
import net.beastcube.minigameapi.team.*;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author BeastCube
 */
@Minigame(arena = BedwarsArena.class, team = BedwarsTeam.class, minigameType = MinigameType.BEDWARS, scoreboardMode = ScoreboardMode.TEAMS_WITH_NO_PLAYERS)
public class Bedwars extends JavaPlugin implements Listener {

    @Getter
    private static MinigameAPI<BedwarsArena, BedwarsTeam, BedwarsConfig> minigameAPI;
    @Getter
    private static Bedwars instance;

    private static Material BRONZE_MATERIAL = Material.CLAY_BRICK, SILVER_MATERIAL = Material.IRON_INGOT, GOLD_MATERIAL = Material.GOLD_INGOT;
    private static String BRONZE_NAME = "Bronze", SILVER_NAME = "Silber", GOLD_NAME = "Gold";

    private static long KILL_POINTS = 5, BED_POINTS = 15, WIN_POINTS = 20;
    private Timestamp gameStartedTimeStamp;

    private static List<Material> materialWhitelist = Arrays.asList(
            //Material.SANDSTONE,
            Material.ENDER_STONE,
            Material.IRON_BLOCK,
            Material.CHEST,
            Material.DROPPER,
            Material.ENDER_CHEST,
            Material.ICE,
            Material.FIRE,
            Material.TNT,
            Material.STAINED_GLASS,
            Material.LADDER,
            Material.BED_BLOCK,
            Material.STAINED_CLAY
    );
    private static List<Location> blockBlacklist = new ArrayList<>();

    public void onEnable() {
        instance = this;

        Defaults defaults = new Defaults();
        defaults.setBlockBreaking(false);
        defaults.setBlockPlacing(false);
        defaults.setCrafting(false);
        defaults.setAutoRespawn(true);
        defaults.setDropStuffOnDeath(false);
        defaults.setDaylightCycle(false);
        defaults.setEnterBedAllowed(false);
        defaults.setDestroySoil(false);
        defaults.setHangingBreak(false);
        defaults.setBlockBurning(false);

        minigameAPI = new MinigameAPI<>(this, new PlayerAmountBounds(PlayerAmountBoundType.MIN_MAX, 2, 32), defaults, BedwarsArena.class, BedwarsTeam.class, new BedwarsConfig());

        //TODO int colorset = new Random().nextInt(TeamColor.getNumberOfSets());
        for (int i = 0; i < ((BedwarsConfig) minigameAPI.getConfig().minigameConfig).numberOfTeams; i++) {
            minigameAPI.addTeam(new BedwarsTeam(TeamColor.getName(i), TeamColor.getColor(i)));
        }

        MapChoosing.setMapChooser(MapChooserInteractItem.getMapChooser());
        //MapChoosing.setMapValidator(new ColorSetValidator(colorset));
        TeamChoosing.setTeamChooser(TeamChooserInteractItem.getTeamChooser());
        KitChoosing.setKitChooser(KitChooserInteractItem.getKitChooser());

        Register.registerEntities();
        Kits.register();

        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getPluginManager().registerEvents(new LaunchBowListener(), this);
        Bukkit.getPluginManager().registerEvents(new SheepListener(), this);
        Bukkit.getPluginManager().registerEvents(new TeamChestHandler(), this);
        Bukkit.getPluginManager().registerEvents(new KamikazeListener(), this);

        Countdowns.removeCountdown(Countdowns.arenaCountdown);
    }

    public void onDisable() {
        Register.removeEntities();
    }

    public static ItemStack getBronze(int amount) {
        ItemStack bronze = new ItemStack(minigameAPI.getArena().getBronzeStack());
        bronze.setAmount(amount);
        return bronze;
    }

    public static ItemStack getSilver(int amount) {
        ItemStack silver = new ItemStack(minigameAPI.getArena().getSilverStack());
        silver.setAmount(amount);
        return silver;
    }

    public static ItemStack getGold(int amount) {
        ItemStack gold = new ItemStack(minigameAPI.getArena().getGoldStack());
        gold.setAmount(amount);
        return gold;
    }

    private boolean canBreak(Block block) {
        return materialWhitelist.contains(block.getType()) && !blockBlacklist.contains(block.getLocation());
    }

    @EventHandler
    public void onChunckLoad(ChunkLoadEvent e) {
        Chunk chunk = e.getChunk();
        World world = chunk.getWorld();

        int bx = chunk.getX() << 4; //Block X
        int bz = chunk.getZ() << 4; //Block Z

        for (int xx = bx; xx < bx + 16; xx++) {
            for (int zz = bz; zz < bz + 16; zz++) {
                for (int yy = 0; yy < world.getMaxHeight(); yy++) {

                    Block block = chunk.getBlock(xx, yy, zz);
                    if (block != null && block.getType() != Material.AIR && block.getType() != Material.BED_BLOCK) {
                        blockBlacklist.add(block.getLocation());
                    }
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        Team team = minigameAPI.getPlayersTeam(p);
        if (team != null && team.isIngame()) {
            Block block = e.getPlayer().getLocation().subtract(0, 1, 0).getBlock();
            if (block.getType() == Material.STAINED_GLASS) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> {
                    if (team.getColor() != ColorConverter.dyeColorToColor(DyeColor.getByData(block.getData())) && !blockBlacklist.contains(block.getLocation())) { //TODO not working?!?!
                        block.getWorld().spawnFallingBlock(block.getLocation(), block.getType(), block.getData());
                        block.setType(Material.AIR);
                    }
                }, 5);
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (minigameAPI.getState() == MinigameState.INGAME) {
            if (materialWhitelist.contains(e.getBlock().getType()))
                e.setCancelled(false);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (minigameAPI.getState() == MinigameState.INGAME) {
            if (canBreak(e.getBlock())) {
                e.setCancelled(false);
                String st = minigameAPI.getArena().isBed(e.getBlock().getLocation());
                if (st != null) {
                    BedwarsTeam t = (BedwarsTeam) minigameAPI.getTeam(st);
                    if (t != null && t.hasBed() && t.isIngame()) {
                        if (minigameAPI.getPlayersTeam(e.getPlayer()) == t) {
                            e.setCancelled(true);
                            e.getPlayer().sendMessage(ChatColor.RED + "Du kannst dein eigenes Bett nicht zerstören!");
                        } else {
                            e.setCancelled(true);
                            e.getBlock().setType(Material.AIR);
                            //Stats
                            Bedwars.getMinigameAPI().getRoundStatsCache().increaseStat(e.getPlayer().getUniqueId(), BedwarsStats.BedwarsStat.BEDS_BROKEN, 1);
                            Bedwars.getMinigameAPI().getRoundStatsCache().increaseStat(e.getPlayer().getUniqueId(), BedwarsStats.BedwarsStat.POINTS, BED_POINTS);
                            sendPointsActionBarMessage(e.getPlayer(), BED_POINTS);
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                Team tm = minigameAPI.getPlayersTeam(p);
                                if (tm == t) {
                                    TitleAPI.sendTitle(p, ChatColor.RED + "Euer Bett wurde zerstört!", ChatColor.GRAY + "von " + ChatColor.GREEN + e.getPlayer().getName());
                                    p.sendMessage(ChatColor.YELLOW + "Euer Bett wurde von " + ChatColor.GREEN + e.getPlayer().getName() + ChatColor.YELLOW + " zerstört!");
                                } else if (e.getPlayer() == p) {
                                    TitleAPI.sendTitle(p, ChatColor.GREEN + "Du hast das Bett von " + t.getChatColor() + t.getName() + ChatColor.YELLOW + " zerstört!", "");
                                    p.sendMessage("Du hast das Bett von " + t.getChatColor() + t.getName() + ChatColor.YELLOW + " zerstört!");
                                } else {
                                    TitleAPI.sendTitle(p, ChatColor.YELLOW + "Das Bett von " + t.getChatColor() + t.getName() + ChatColor.YELLOW + " wurde zerstört!", ChatColor.GRAY + "von " + e.getPlayer().getName());
                                    p.sendMessage(ChatColor.YELLOW + "Das Bett von " + t.getChatColor() + t.getName() + ChatColor.YELLOW + " wurde von " + ChatColor.GREEN + e.getPlayer().getName() + ChatColor.YELLOW + " zerstört!");
                                }
                                p.playSound(p.getLocation(), Sound.BLAZE_DEATH, 100f, 1f);
                            }
                            t.setHasBed(false);
                            ScoreboardManager.updateScoreboard();
                        }
                    } else {
                        e.setCancelled(true);
                        e.getPlayer().sendMessage(ChatColor.RED + "Dieses Team spielt nicht mit!");
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e) {
        List<Block> blocks = e.blockList().stream().filter(block -> !canBreak(block) || block.getType() == Material.BED_BLOCK).collect(Collectors.toList());
        e.blockList().removeAll(blocks);
    }

    @EventHandler
    public void onGameStart(GameStartEvent e) {
        gameStartedTimeStamp = new Timestamp(System.currentTimeMillis());
        Spawner.start();
        for (Player p : minigameAPI.getPlayersIngame()) {
            Team team = minigameAPI.getPlayersTeam(p);
            p.sendMessage(ChatColor.GOLD + "Das Spiel hat begonnen! Du bist im Team " + ColorConverter.colorToChatColor(team.getColor()) + team.getName());
        }
    }

    @EventHandler
    public void onPlayerIngameDeath(PlayerIngameDeathEvent e) {
        ScoreboardManager.updateScoreboard();

        final Player p = e.getPlayer();
        p.getInventory().clear();
        ItemStack EMPTY = new ItemStack(Material.AIR);
        p.getInventory().setArmorContents(new ItemStack[]{
                EMPTY, EMPTY, EMPTY, EMPTY
        });

        p.playSound(p.getLocation(), Sound.FALL_BIG, 25f, 1f);

        BedwarsTeam team = (BedwarsTeam) minigameAPI.getPlayersTeam(p);
        Player killer = p.getKiller();
        if (killer != null) {
            Team pt = minigameAPI.getPlayersTeam(killer);
            if (pt != null)
                e.setDeathMessage(team.getChatColor() + p.getName() + ChatColor.GRAY + " wurde von " + pt.getChatColor() + killer.getName() + ChatColor.GRAY + " getötet.");
            if (!team.hasBed()) {
                Bedwars.getMinigameAPI().getRoundStatsCache().increaseStat(killer.getUniqueId(), BedwarsStats.BedwarsStat.KILLS, 1);
                Bedwars.getMinigameAPI().getRoundStatsCache().increaseStat(killer.getUniqueId(), BedwarsStats.BedwarsStat.POINTS, KILL_POINTS);
                sendPointsActionBarMessage(killer, KILL_POINTS);
            }
        } else {
            e.setDeathMessage(team.getChatColor() + p.getName() + ChatColor.GRAY + " ist gestorben!");
        }

        if (!team.hasBed()) {
            Bedwars.getMinigameAPI().getRoundStatsCache().setStat(p.getUniqueId(), BedwarsStats.BedwarsStat.DEATHS, 1);
            setGameEndedStats(p);
            Bedwars.getMinigameAPI().getRoundStatsCache().saveStats(p.getUniqueId());
            minigameAPI.setPlayerState(p, PlayerState.LOST);
            minigameAPI.getPlayersTeam(p).removePlayer(e.getPlayer());
        }
    }

    @EventHandler
    public void onTeamStateChanged(TeamStateChangedEvent e) {
        if (e.getState() == PlayerState.LOST) {
            List<Team> teamsIngame = minigameAPI.getTeamsIngame();
            if (teamsIngame.size() == 1) {
                minigameAPI.endGame(teamsIngame.get(0));
            } else if (teamsIngame.size() == 0) {
                minigameAPI.endGame(null);
            } else {
                e.setBroadcastTitleMessage(new TitleObject(e.getTeam().getChatColor() + e.getTeam().getName() + " ist ausgeschieden!", ""));
            }
        }
    }

    @EventHandler
    public void onTeamPlayerCountChanged(TeamPlayerCountChangedEvent e) {
        if (e.getPlayerCount() == 0 && minigameAPI.getState() == MinigameState.INGAME) {
            List<Team> teamsIngame = minigameAPI.getTeamsIngame();
            if (teamsIngame.size() == 1) {
                minigameAPI.endGame(teamsIngame.get(0));
            } else if (teamsIngame.size() == 0) {
                minigameAPI.endGame(null);
            } else {
                e.setBroadcastTitleMessage(new TitleObject(e.getTeam().getChatColor() + e.getTeam().getName() + " ist ausgeschieden!", ""));
            }
        }
    }

    @EventHandler
    public void onPlayerIngameQuit(final PlayerIngameQuitEvent e) {
        if (minigameAPI.getState() == MinigameState.INGAME) {
            UUID uniqueId = e.getPlayer().getUniqueId();
            Bedwars.getMinigameAPI().getRoundStatsCache().setStat(uniqueId, BedwarsStats.BedwarsStat.POINTS, -50);
            Bedwars.getMinigameAPI().getRoundStatsCache().setStat(uniqueId, BedwarsStats.BedwarsStat.DEATHS, 1);
            setGameEndedStats(e.getPlayer());
            Bedwars.getMinigameAPI().getRoundStatsCache().saveStats(uniqueId);
        }
    }

    private void setGameEndedStats(Player p) {
        Bedwars.getMinigameAPI().getRoundStatsCache().setStat(p.getUniqueId(), BedwarsStats.BedwarsStat.GAMES_PLAYED, 1);
        Bedwars.getMinigameAPI().getRoundStatsCache().setStat(p.getUniqueId(), BedwarsStats.BedwarsStat.LAST_PLAYED, System.currentTimeMillis());

        long millis = System.currentTimeMillis() - gameStartedTimeStamp.getTime();
        Bedwars.getMinigameAPI().getRoundStatsCache().setStat(p.getUniqueId(), BedwarsStats.BedwarsStat.PLAYING_TIME, TimeUnit.MILLISECONDS.toMinutes(millis));
        p.sendMessage(ChatColor.YELLOW + "----------------  Stats  ----------------");
        Optional<Map<Stat, Long>> map = Bedwars.getMinigameAPI().getRoundStatsCache().getStats(p.getUniqueId()); //TODO minigametype.getStats().format(map);
        if (map.isPresent()) {
            map.get().forEach((stat, value) -> p.sendMessage(stat.getDisplayName() + ": +" + ChatColor.GOLD + stat.format(value))); //TODO Last played
        }
    }

    private void sendPointsActionBarMessage(Player p, long points) {
        ActionBarAPI.sendActionBar(p, ChatColor.DARK_GREEN + "+" + points + ChatColor.DARK_GRAY + " Punkte");
    }

    @EventHandler
    public void onGameEnd(GameEndEvent e) {
        if (e.getWinnerTeam() != null) {
            e.getMessage().setSubtitle(ChatColor.GOLD + "Das Team " + e.getWinnerTeam().getName() + " hat gewonnen!");
        }
        for (Player p : e.getWinners()) {
            Bedwars.getMinigameAPI().getRoundStatsCache().increaseStat(p.getUniqueId(), BedwarsStats.BedwarsStat.GAMES_WON, 1);
            Bedwars.getMinigameAPI().getRoundStatsCache().increaseStat(p.getUniqueId(), BedwarsStats.BedwarsStat.POINTS, WIN_POINTS);
            sendPointsActionBarMessage(p, WIN_POINTS);
        }
        minigameAPI.getPlayersIngame().forEach(p -> {
            setGameEndedStats(p);
            Bedwars.getMinigameAPI().getRoundStatsCache().saveStats(p.getUniqueId());
        });
    }

    @EventHandler
    public void onPlayerInteractFakeMob(PlayerInteractFakeMobEvent e) {
        if (minigameAPI.getState() == MinigameState.INGAME && e.getMob().getType() == EntityType.VILLAGER && e.getAction() == PlayerInteractFakeMobEvent.Action.RIGHT_CLICK) {
            ShopHandler.showShop(e.getPlayer());
        }
    }
}
