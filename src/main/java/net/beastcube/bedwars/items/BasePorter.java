package net.beastcube.bedwars.items;

import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.effect.CylinderEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.bedwars.Bedwars;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * @author BeastCube
 */
public class BasePorter extends InteractItem {

    private static BasePorter singleton;

    public static BasePorter getBasePorter() {
        if (singleton == null)
            singleton = new BasePorter();
        return singleton;
    }

    private BasePorter() {
        InteractItemManager.registerInteractItem(this);
    }

    @Override
    public Material getMaterial() {
        return Material.EYE_OF_ENDER;
    }

    @Override
    public String getDisplayName() {
        return ChatColor.DARK_PURPLE + "Zupfde Hoam";
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(final Player p, JavaPlugin plugin, Object data) {
        if (Bedwars.getMinigameAPI().getState() == MinigameState.INGAME && p.isOnGround()) {
            if (!tasks.containsKey(p)) {
                ItemStack item = p.getItemInHand().clone();
                item.setAmount(1);
                p.getInventory().removeItem(item);
                startTask(p, item);
            } else {
                p.sendMessage(ChatColor.RED + "Wird gerade benutzt!");
            }
        } else {
            p.sendMessage(ChatColor.RED + "Du musst auf dem Boden sein!");
        }
    }

    HashMap<Player, Integer> tasks = new HashMap<>();

    private void startTask(final Player p, final ItemStack item) {
        final Location iL = p.getLocation();
        final Runnable myrunnable = new Runnable() {
            int counter = 0;

            @Override
            public void run() {
                counter++;
                Location loc = p.getLocation();
                if (iL.getBlockX() == loc.getBlockX() && iL.getBlockY() == loc.getBlockY() && iL.getBlockZ() == loc.getBlockZ()) {
                    if (counter < 20) {
                        if (counter % 2 == 0) {
                            CylinderEffect effect = new CylinderEffect(BeastCubeAPI.effectManager);
                            DynamicLocation location = new DynamicLocation(p.getLocation());
                            location.addOffset(new Vector(0, counter / 8, 0));
                            effect.setDynamicOrigin(location);
                            effect.particle = ParticleEffect.FIREWORKS_SPARK;
                            effect.particles = 20;
                            effect.speed = 0;
                            effect.type = EffectType.INSTANT;
                            effect.height = counter / 8;
                            effect.visibleRange = 32;
                            effect.solid = true;
                            effect.enableRotation = false;
                            BeastCubeAPI.effectManager.start(effect);
                            p.getWorld().playSound(p.getLocation(), Sound.ORB_PICKUP, 50f, 1f);
                        }
                    } else {
                        p.teleport(Bedwars.getMinigameAPI().getArena().getSpawn(p));
                        Bukkit.getScheduler().cancelTask(tasks.get(p));
                        tasks.remove(p);
                    }
                } else {
                    p.sendMessage(ChatColor.RED + "Teleportation abgebrochen");
                    p.getInventory().addItem(item);
                    Bukkit.getScheduler().cancelTask(tasks.get(p));
                    tasks.remove(p);
                }
            }
        };
        tasks.put(p, Bukkit.getScheduler().scheduleSyncRepeatingTask(Bedwars.getMinigameAPI().getMinigame(), myrunnable, 5, 5));
    }

}
