package net.beastcube.bedwars.items;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.projectiles.ProjectileSource;

/**
 * @author BeastCube
 */
public class LaunchBowListener implements Listener {

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
        if (e.getPlayer().getItemInHand().getType() == Material.BOW) {
            if (e.getPlayer().getItemInHand().hasItemMeta()) {
                if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_PURPLE + "Wurfbogen")) {
                    if (e.getPlayer().getPassenger() == null) {
                        Entity other = e.getRightClicked();
                        if (other instanceof Player) {
                            e.getPlayer().setPassenger(other);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent e) {
        Projectile arrow = e.getEntity();
        ProjectileSource shooter = arrow.getShooter();
        if (shooter instanceof Player) {
            Player p = (Player) shooter;
            if (p.getItemInHand().getType() == Material.BOW) {
                if (p.getItemInHand().hasItemMeta()) {
                    if (p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_PURPLE + "Wurfbogen")) {
                        if (p.getPassenger() != null) {

                            Entity passenger = p.getPassenger();
                            p.setPassenger(null);
                            arrow.setPassenger(passenger);
                        }
                    }
                }
            }
        }
    }
}
