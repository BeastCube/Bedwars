package net.beastcube.bedwars.items;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class KamikazeListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE && e.getSlotType() == InventoryType.SlotType.ARMOR) {
            e.getWhoClicked().getWorld().createExplosion(e.getWhoClicked().getLocation(), 2.5f, false);
            e.setCancelled(true);
            e.getWhoClicked().getInventory().setChestplate(new ItemStack(Material.AIR));
        }
    }

}
