package net.beastcube.bedwars.items.sheep;

import net.beastcube.bedwars.Bedwars;
import net.beastcube.minigameapi.team.ColorConverter;
import net.beastcube.minigameapi.team.Team;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author BeastCube
 */
public class Register {

    private static List<Sheeep> sheeps = new ArrayList<>();
    private static List<EntityTNTPrimed> tnts = new ArrayList<>();

    public static boolean registerEntities() {
        try {
            Class<EntityTypes> entityTypeClass = EntityTypes.class;

            Field c = entityTypeClass.getDeclaredField("c");
            c.setAccessible(true);
            HashMap c_map = (HashMap) c.get(null);
            c_map.put("Sheeep", Sheeep.class);

            Field d = entityTypeClass.getDeclaredField("d");
            d.setAccessible(true);
            HashMap d_map = (HashMap) d.get(null);
            d_map.put(Sheeep.class, "Sheeep");

            Field e = entityTypeClass.getDeclaredField("e");
            e.setAccessible(true);
            HashMap e_map = (HashMap) e.get(null);
            e_map.put(91, Sheeep.class);

            Field f = entityTypeClass.getDeclaredField("f");
            f.setAccessible(true);
            HashMap f_map = (HashMap) f.get(null);
            f_map.put(Sheeep.class, 91);

            Field g = entityTypeClass.getDeclaredField("g");
            g.setAccessible(true);
            HashMap g_map = (HashMap) g.get(null);
            g_map.put("Sheeep", 91);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void removeEntities() {
        for (Sheeep sheep : sheeps) {
            sheep.die();
        }
        for (EntityTNTPrimed tnt : tnts) {
            tnt.die();
        }
    }

    @SuppressWarnings("deprecation")
    public static Sheeep spawnSheep(Bedwars m, final Location loc, Player target, final ChatColor color) {
        final Object w = ((CraftWorld) loc.getWorld()).getHandle();
        final Sheeep t_ = new Sheeep(((CraftWorld) loc.getWorld()).getHandle(), ((CraftPlayer) target).getHandle(), loc, true);

        Bukkit.getScheduler().runTask(m, () -> {
            // t_.id = Block.getById(35);
            // t_.data = color;
            ((World) w).addEntity(t_, CreatureSpawnEvent.SpawnReason.CUSTOM);
            t_.setColor(EnumColor.fromColorIndex(ColorConverter.chatColorToDyeColor(color).getWoolData()));
            t_.setPosition(loc.getX(), loc.getY(), loc.getZ());
        });

        return t_;
    }

    public static void spawnSheep(final Location l, final Player p) {
        final Team team = Bedwars.getMinigameAPI().getPlayersTeam(p);
        Player target = calculateTarget(l, team);
        if (target != null) {
            int delay = 20 * 10;
            final Sheeep s = spawnSheep(Bedwars.getInstance(), l.add(0D, 1D, 0D), target, ColorConverter.colorToChatColor(team.getColor()));
            sheeps.add(s);
            final ItemStack item = p.getItemInHand();
            l.getBlock().getChunk().load();
            Bukkit.getScheduler().runTaskLater(Bedwars.getInstance(), () -> {
                ItemStack removeitem = item.clone();
                removeitem.setAmount(1);
                p.getInventory().removeItem(removeitem);
                p.updateInventory();
            }, 5L);
            final BukkitTask task = Bukkit.getScheduler().runTaskTimer(Bedwars.getInstance(), () -> {
                removeTNT(s.passenger);
                Bukkit.getScheduler().runTaskLater(Bedwars.getInstance(), () -> {
                    final EntityTNTPrimed tnt = spawnTNT(p, s.getLocation());
                    tnt.mount(s);
                }, 1L);
            }, 0L, 60L);
            Bukkit.getScheduler().runTaskLater(Bedwars.getInstance(), () -> {
                Location l1 = s.getLocation();
                l1.getWorld().createExplosion(l1.getX(), l1.getY(), l1.getZ(), 4.5F, false, true);
                removeTNT(s.passenger);
                s.die();
                sheeps.remove(s);
                task.cancel();
            }, delay);
            Bukkit.getScheduler().runTaskTimer(Bedwars.getInstance(), () -> {
                Player target1 = calculateTarget(l, team);
                if (target1 != null) {
                    s.setTarget(target1);
                }
            }, 0, 20L);

        }
    }

    public static boolean removeTNT(Entity tnt) {
        if (tnt != null && tnt instanceof EntityTNTPrimed) {
            tnt.die();
            if (tnts.contains(tnt)) {
                tnts.remove(tnt);
                return true;
            }
        }
        return false;
    }

    public static EntityTNTPrimed spawnTNT(Player p, Location l) {
        World world = ((CraftWorld) p.getWorld()).getHandle().b();
        final EntityTNTPrimed tnt = new EntityTNTPrimed(world, l.getX(), l.getY() + 1, l.getZ(), null);
        tnt.yield = 0;
        world.addEntity(tnt, CreatureSpawnEvent.SpawnReason.CUSTOM);
        tnts.add(tnt);
        return tnt;
    }

    public static Player calculateTarget(Location l, Team team) {
        Player target = null;
        int temp_dist = 0;
        for (Player players : Bukkit.getOnlinePlayers()) {
            if (Bedwars.getMinigameAPI().getPlayersTeam(players) != team && Bedwars.getMinigameAPI().isIngame(players)) {
                if (players.getLocation().getWorld() == l.getWorld()) {
                    int dist = (int) players.getLocation().distanceSquared(l);
                    if (dist < temp_dist || target == null) {
                        temp_dist = dist;
                        target = players;
                    }
                }
            }
        }
        return target;
    }

}