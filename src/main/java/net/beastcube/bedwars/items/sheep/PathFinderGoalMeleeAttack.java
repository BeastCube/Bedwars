package net.beastcube.bedwars.items.sheep;

import net.minecraft.server.v1_8_R3.EntityCreature;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;

/**
 * @author BeastCube
 */
public class PathFinderGoalMeleeAttack extends PathfinderGoalMeleeAttack {

    EntityCreature b;

    public PathFinderGoalMeleeAttack(EntityCreature entitycreature, double d0, boolean flag) {
        super(entitycreature, d0, flag);
        b = entitycreature;
    }

    public PathFinderGoalMeleeAttack(EntityCreature entitycreature, Class c, double d0, boolean flag) {
        super(entitycreature, c, d0, flag);
        b = entitycreature;
    }

    @Override
    public void e() {
        b.getNavigation().a(b.getGoalTarget());
    }

}
