package net.beastcube.bedwars.items.sheep;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.entity.EntityTargetEvent;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * @author BeastCube
 */
public class Sheeep extends EntitySheep {

    private final Entity target;
    private boolean invulnerable;

    public Sheeep(World world, Entity target, Location loc, boolean invulnerable) {
        super(world);
        this.invulnerable = invulnerable;
        NBTTagCompound tag = new NBTTagCompound();
        this.e(tag);
        tag.setBoolean("Invulnerable", invulnerable);
        this.f(tag);
        this.locX = loc.getX();
        this.locY = loc.getY();
        this.locZ = loc.getZ();
        try {
            Field b = this.goalSelector.getClass().getDeclaredField("b");
            b.setAccessible(true);
            b.set(this.goalSelector, new ArrayList());
            this.getAttributeInstance(GenericAttributes.FOLLOW_RANGE).setValue(128D);
            this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.37D);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.goalSelector.a(0, new PathFinderGoalMeleeAttack(this, EntityHuman.class, 1D, false));
        // this.goalSelector.a(0, new PathfinderGoalFollowParent(this, 1.1D));
        this.target = target;
        setTarget(target);
    }

    public Location getLocation() {
        return new Location(this.world.getWorld(), this.locX, this.locY, this.locZ);
    }


    public Entity getTarget() {
        return this.target;
    }

    public void setTarget(Entity target) {
        this.setGoalTarget((EntityLiving) target, EntityTargetEvent.TargetReason.OWNER_ATTACKED_TARGET, false);
        ((Sheep) this.getBukkitEntity()).setTarget((LivingEntity) target.getBukkitEntity());
    }

    public void setTarget(Player target) {
        setTarget(((CraftPlayer) target).getHandle());
    }

    //Disabled Damage by player
    @Override
    public boolean isInvulnerable(DamageSource damagesource) {
        return invulnerable && damagesource != DamageSource.OUT_OF_WORLD;
    }

    //Disabled mob collision
    @Override
    public void collide(Entity entity) {

    }

}