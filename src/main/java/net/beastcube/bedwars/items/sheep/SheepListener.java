package net.beastcube.bedwars.items.sheep;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @author BeastCube
 */
public class SheepListener implements Listener {

    @EventHandler
    public void onInteract(final PlayerInteractEvent e) {
        if (e.hasItem()) {
            if (e.getItem().getType() == Material.MONSTER_EGG && e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (e.hasBlock()) {
                    final Location l = e.getClickedBlock().getLocation();
                    Register.spawnSheep(l, e.getPlayer());
                }
                e.setCancelled(true);
            }
        }
    }

}

