package net.beastcube.bedwars;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.beastcube.api.bukkit.commands.Arg;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import net.beastcube.minigameapi.arena.Status;
import net.beastcube.minigameapi.arena.TeamArena;
import net.beastcube.minigameapi.util.SmoothSerializer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class BedwarsArena extends TeamArena {

    @Serialize
    @Status(name = "Bronzespawner", command = "/addBronze")
    @Getter
    @Setter
    List<Location> bronze = new ArrayList<>();
    @Serialize
    @Status(name = "Silberspawner", command = "/addSilver")
    @Getter
    @Setter
    List<Location> silver = new ArrayList<>();
    @Serialize
    @Status(name = "Goldspawner", command = "/addGold")
    @Getter
    @Setter
    List<Location> gold = new ArrayList<>();
    @Serialize
    @Status(name = "Bronze", command = "/setBronzeStack")
    @Getter
    @Setter
    ItemStack bronzeStack = new ItemBuilder(Material.CLAY_BRICK).name(ChatColor.DARK_RED + "Bronze").build();
    @Serialize
    @Status(name = "Silber", command = "/setSilverStack")
    @Getter
    @Setter
    ItemStack silverStack = new ItemBuilder(Material.IRON_INGOT).name(ChatColor.GRAY + "Silber").build();
    @Serialize
    @Status(name = "Gold", command = "/setGoldStack")
    @Getter
    @Setter
    ItemStack goldStack = new ItemBuilder(Material.GOLD_INGOT).name(ChatColor.GOLD + "Gold").build();
    @Serialize
    @Status(name = "Bronze-Häufigkeit", command = "/setBronzeFrequency")
    @Getter
    @Setter
    int bronzeFreq = 20;
    @Serialize
    @Status(name = "Silver-Häufigkeit", command = "/setSilverFrequency")
    @Getter
    @Setter
    int silverFreq = 6 * 20;
    @Serialize
    @Status(name = "Gold-Häufigkeit", command = "/setGoldFrequency")
    @Getter
    @Setter
    int goldFreq = 40 * 20;
    @Serialize
    @Status(name = "Betten", command = "/setBed")
    Map<String, List<Location>> beds = new HashMap<>();

    public BedwarsArena() {
        super();
    }

    public String isBed(Location loc) {
        for (Map.Entry<String, List<Location>> e : beds.entrySet()) {
            for (Location l : e.getValue()) {
                if (l.getBlockX() == loc.getBlockX() && l.getBlockY() == loc.getBlockY() && l.getBlockZ() == loc.getBlockZ())
                    return e.getKey();
            }
        }
        return null;
    }

    public void addBronze(Location loc) {
        bronze.add(loc);
    }

    public void removeBronze(Location loc) {
        for (int i = 0; i < bronze.size(); i++) {
            Location b = bronze.get(i);
            if (b.getBlockX() == loc.getBlockX() && b.getBlockY() == loc.getBlockY() && b.getBlockZ() == loc.getBlockZ())
                bronze.remove(i);
        }
    }

    public void clearBronze() {
        bronze.clear();
    }

    @Command(identifiers = "addBronze", description = "Fügt einen neuen Bronzespawner hinzu")
    public void addBronze(Player sender) {
        addBronze(sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Es wurde ein neuer Bronzespawner an deiner aktuellen Position hinzugefügt.");
    }

    @Command(identifiers = "removeBronze", description = "Löscht den nächsten Bronzespawner")
    public void removeBronze(Player sender) {
        removeNearestSpawner(bronze, sender);
    }

    @Command(identifiers = "clearBronze", description = "Löscht alle Bronzespawner")
    public void clearBronze(Player sender) {
        clearBronze();
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich alle Bronzespawner entfernt.");
    }

    public void addSilver(Location loc) {
        silver.add(loc);
    }

    public void removeSilver(Location loc) {
        for (int i = 0; i < silver.size(); i++) {
            Location b = silver.get(i);
            if (b.getBlockX() == loc.getBlockX() && b.getBlockY() == loc.getBlockY() && b.getBlockZ() == loc.getBlockZ())
                silver.remove(i);
        }
    }

    public void clearSilver() {
        silver.clear();
    }

    @Command(identifiers = "addSilver", description = "Fügt einen neuen Silberspawner hinzu")
    public void addSilver(Player sender) {
        addSilver(sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Es wurde ein neuer Silberspawner an deiner aktuellen Position hinzugefügt.");
    }

    @Command(identifiers = "removeSilver", description = "Löscht den nächsten Silberspawner")
    public void removeSilver(Player sender) {
        removeNearestSpawner(silver, sender);
    }

    @Command(identifiers = "clearSilver", description = "Löscht alle Silberspawner")
    public void clearSilver(Player sender) {
        clearSilver();
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich alle Silberspawner entfernt.");
    }

    public void addGold(Location loc) {
        gold.add(loc);
    }

    public void removeGold(Location loc) {
        for (int i = 0; i < gold.size(); i++) {
            Location b = gold.get(i);
            if (b.getBlockX() == loc.getBlockX() && b.getBlockY() == loc.getBlockY() && b.getBlockZ() == loc.getBlockZ())
                gold.remove(i);
        }
    }

    public void clearGold() {
        gold.clear();
    }

    @Command(identifiers = "addGold", description = "Fügt einen neuen Goldspawner hinzu")
    public void addGold(Player sender) {
        addGold(sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Es wurde ein neuer Goldspawner an deiner aktuellen Position hinzugefügt.");
    }

    @Command(identifiers = "removeGold", description = "Löscht den nächsten Goldspawner")
    public void removeGold(Player sender) {
        removeNearestSpawner(gold, sender);
    }

    @Command(identifiers = "clearGold", description = "Löscht alle Goldspawner")
    public void clearGold(Player sender) {
        clearGold();
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich alle Goldspawner entfernt.");
    }

    @Command(identifiers = "setBronzeStack", description = "Setzt das Item, welches für Bronze gespawnt werden wird")
    public void setBronzeStackCommand(Player sender) {
        if (sender.getItemInHand() != null) {
            setBronzeStack(sender.getItemInHand());
            sender.sendMessage(ChatColor.GREEN + "Das Item für Bronze wurde erfolgreich gesetzt.");
        } else
            sender.sendMessage(ChatColor.RED + "Du musst ein Item in der Hand halten");
    }

    @Command(identifiers = "setSilverStack", description = "Setzt das Item, welches für Silber gespawnt werden wird")
    public void setSilverStackCommand(Player sender) {
        if (sender.getItemInHand() != null) {
            setSilverStack(sender.getItemInHand());
            sender.sendMessage(ChatColor.GREEN + "Das Item für Silber wurde erfolgreich gesetzt.");
        } else
            sender.sendMessage(ChatColor.RED + "Du musst ein Item in der Hand halten");
    }

    @Command(identifiers = "setGoldStack", description = "Setzt das Item, welches für Gold gespawnt werden wird")
    public void setGoldStackCommand(Player sender) {
        if (sender.getItemInHand() != null) {
            setGoldStack(sender.getItemInHand());
            sender.sendMessage(ChatColor.GREEN + "Das Item für Gold wurde erfolgreich gesetzt.");
        } else
            sender.sendMessage(ChatColor.RED + "Du musst ein Item in der Hand halten");
    }

    @Command(identifiers = "setBronzeFrequency", description = "Setzt die Spawnrate von Bronze in Ticks.")
    public void setBronzeFreq(Player sender, @Arg(name = "Häufigkeit", verifiers = "min[1]") int freq) {
        setBronzeFreq(freq);
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich die Spawnrate von Bronze auf " + freq + " Ticks gesetzt.");
    }

    @Command(identifiers = "setSilverFrequency", description = "Setzt die Spawnrate von Silber in Tick.")
    public void setSilverFreq(Player sender, @Arg(name = "Häufigkeit", verifiers = "min[1]") int freq) {
        setSilverFreq(freq);
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich die Spawnrate von Silber auf " + freq + " Ticks gesetzt.");
    }

    @Command(identifiers = "setGoldFrequency", description = "Setzt die Spawnrate von Gold in Tick.")
    public void setGoldFreq(Player sender, @Arg(name = "Häufigkeit", verifiers = "min[1]") int freq) {
        setGoldFreq(freq);
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich die Spawnrate von Gold auf " + freq + " Ticks gesetzt.");
    }

    @SuppressWarnings("deprecation")
    @Command(identifiers = "setBed", description = "Setzt das Bett für ein Team")
    public void setBed(Player sender, @Arg(name = "Team") String team) {
        List<Location> both = getBothBedComponents(sender.getTargetBlock((HashSet<Byte>) null, 10));
        if (both == null) {
            sender.sendMessage(ChatColor.RED + "Zu weit entfernt!");
        } else {
            beds.put(team, both);
            sender.sendMessage(ChatColor.GREEN + "Das Bett für " + team + " wurde erfolgreich gesetzt");
        }
    }

    @Command(identifiers = "removeBed", description = "Entfernt das Bett für ein Team")
    public void removeBed(Player sender, @Arg(name = "Team") String team) {
        if (beds.containsKey(team)) {
            beds.remove(team);
            sender.sendMessage(ChatColor.GREEN + "Das Bett für " + team + " wurde erfolgreich entfernt.");
        } else {
            sender.sendMessage(ChatColor.RED + "Das Team hat kein Bett");
        }
    }


    private void removeNearestSpawner(List<Location> spawners, Player sender) {
        if (spawners.size() == 0)
            sender.sendMessage(ChatColor.RED + "Es existiert noch kein Spawn.");
        Location nearest = spawners.get(0);
        double smallestDistance = nearest.distanceSquared(sender.getLocation());
        double currentDistance;
        for (int i = 1; i < spawners.size(); i++) {
            currentDistance = spawners.get(i).distanceSquared(sender.getLocation());
            if (currentDistance < smallestDistance) {
                smallestDistance = currentDistance;
                nearest = spawners.get(i);
            }
        }
        if (smallestDistance > 9) {
            sender.sendMessage(ChatColor.YELLOW + "Der naheliegendste Spawnpunkt ist mehr als 3 Blöcke entfernt.");
            sender.sendMessage(ChatColor.YELLOW + "Er befindet sich bei: " + SmoothSerializer.serializeVector(nearest.toVector(), 2));
        } else {
            spawners.remove(nearest);
            sender.sendMessage(ChatColor.GREEN + "Der naheliegendste Spawnpunkt wurde erfolgreich entfernt.");
        }
    }

    private List<Location> getBothBedComponents(Block b) {
        List<Location> result = new ArrayList<Location>();

        if (b != null) {
            result.add(b.getLocation());
            Block b2;
            if (b.getType() == Material.BED_BLOCK) {
                for (int x = -1; x <= 1; x++) {
                    for (int z = -1; z <= 1; z++) {
                        b2 = b.getWorld().getBlockAt(b.getLocation().add(x, 0, z));
                        if (b2.getType() == Material.BED_BLOCK && !(x == 0 && z == 0))
                            result.add(b2.getLocation());
                    }
                }
            } else if (b.getType() == Material.ACACIA_DOOR || b.getType() == Material.BIRCH_DOOR || b.getType() == Material.DARK_OAK_DOOR ||
                    b.getType() == Material.IRON_DOOR || b.getType() == Material.JUNGLE_DOOR || b.getType() == Material.SPRUCE_DOOR || b.getType() == Material.WOODEN_DOOR) {
                result.add(b.getLocation().add(0, 1, 0));
            }
            return result;
        } else {
            return null;
        }
    }

}
